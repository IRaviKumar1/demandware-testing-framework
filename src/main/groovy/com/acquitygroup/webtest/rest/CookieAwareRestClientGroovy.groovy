package com.acquitygroup.webtest.rest

import com.google.common.collect.ImmutableMap
import com.google.common.collect.Maps
import groovyx.net.http.ContentEncoding
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import groovyx.net.http.ResponseParseException
import org.apache.http.Header
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CookieAwareRestClientGroovy extends RESTClient {

    private static final Logger LOG = LoggerFactory.getLogger(CookieAwareRestClientGroovy.class)

    private List<String> cookies
    private String authorizationToken

    CookieAwareRestClientGroovy(String defaultURI) {
        setContentEncoding(ContentEncoding.Type.GZIP, ContentEncoding.Type.DEFLATE);
        setUri(defaultURI)
        cookies = []
    }

    @Override
    void setHeaders(Map<?, ?> headers) {
        Map<?, ?> localHeaders = Maps.newHashMap(headers)

        if (cookies.size() > 0) {
            localHeaders['Cookie'] = cookies.join(';')
        }

        super.setHeaders(localHeaders)
    }

    @Override
    protected HttpResponseDecorator defaultSuccessHandler(HttpResponseDecorator resp, Object data) throws ResponseParseException {

        resp.setData(data);

        Header[] headers = resp.getAllHeaders()
        resp.setHeaders(headers)

        resp.getHeaders('Set-Cookie').each {
            //[Set-Cookie: JSESSIONID=E68D4799D4D6282F0348FDB7E8B88AE9; Path=/frontoffice/; HttpOnly]
            String cookie = it.value.split(';')[0]
            LOG.debug("Adding cookie to collection: $cookie")
            cookies.add(cookie)
        }

        resp
    }

    public void clearSession() {
        cookies = []
    }

    public boolean hasSession() {
        cookies.size() > 0
    }

    public void setAuthenticationToken() {
        // TODO: Externalize these properties
        OAuthConsumer consumer = new OAuthConsumer("ce06f440-9bda-495b-81a2-8a2d525f5346", "xb6V74j25a3PbkMD6aeszVHef")
        OAuthProvider provider = new OAuthProvider("https://account.demandware.com/dw/oauth2/access_token")

        String token = provider.retrieveToken(consumer)
        if (token == null) {
            throw new AuthorizationException("Authorization Failed")
        }

        LOG.debug("Auth Token: {}", token)

        authorizationToken = token

        ImmutableMap.Builder builder = ImmutableMap.builder()
        // do not add authorization twice
        getHeaders().each { header ->
            if (header.getKey() != "Authorization") {
                builder.put(header)
            }
        }

        builder.put("Authorization", "Bearer ${authorizationToken}")
        setHeaders(builder.build())
    }

    String getAuthToken() {
        authorizationToken
    }
}
