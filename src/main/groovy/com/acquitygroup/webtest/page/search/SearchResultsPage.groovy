package com.acquitygroup.webtest.page.search

import geb.Page

class SearchResultsPage extends Page {
	
	//REVIEWME: there might be a way to pass in a parameter so as not to hard code the parameter value here
	static url = "default/Search-Show?q=ddd"
	static at = { $("div.section-header p",0).text().startsWith("WE'RE SORRY, NO PRODUCTS WERE FOUND") }
	
	static content = {
		searchResultsBox  { $("input[name=q]", class:"input-text") }
		searchResultsButton  { $("button", name:"simplesearch") }
	}
}