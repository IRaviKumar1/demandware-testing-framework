package com.acquitygroup.webtest.page.search

import geb.Page

class SearchPage extends Page {
	static url = "default/Search-Show"
	static at = { $("span", class: "breadcrumb-result-text").text().startsWith("Your search Results for") }
}


