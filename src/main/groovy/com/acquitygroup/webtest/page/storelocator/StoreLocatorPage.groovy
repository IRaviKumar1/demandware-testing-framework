package com.acquitygroup.webtest.page.storelocator;

import geb.Page

class StoreLocatorPage extends Page {

	static url = "default/Stores-Find";

	static at = {
		waitFor('slow') {
			$("div#storecallout h1").text().contains("Find a Store Near You")
			$("div.gmnoprint").displayed
		}
	}

	static content = {
	
		locationInput {$("input#address")}
		searchButton {$("button.button-fancy-medium")}
		storesContainer{$("div#stores")}
		storeID {String id -> storesContainer.find("div#store${id}")}
		
	}
	

}

