package com.acquitygroup.webtest.page.businessManager

import geb.Page

class bmLandingPage extends Page {

	static url = "https://acquitygroup02.alliance-prtnr-na01.dw.demandware.net/on/demandware.store/Sites-Site/default/ViewApplication-DisplayWelcomePage#BM_Callout";

	static at = {
		waitFor {
			title.contains("Demandware Business Manager")
			$("div#bm_header_instance").text().contains("Instance")
		}
		
	}

	static content = {
		leftNavLink{String content ->$("td.leftmenutxt a", text: contains(content))}
		overviewTitle {String title -> $("td.overview_title").text().contains(title)}
		subtitleLink (required: false){String link -> $("td.overview_subtitle a", text: contains(link))}
		breadcrumb (required: false){String link -> $("td.breadcrumb").text().contains(link)}
		innerLink (required: false){String link -> $("tr td a", href: contains(link)) }
		refreshProfiler (required: false){$("input.button", name: "refresh")}
		resetSensors (required: false) {$("input.button", name: "clear")}
		backButton (required: false){$("input.button", name:"back")}
		
	}

}
