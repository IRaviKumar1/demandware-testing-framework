package com.acquitygroup.webtest.page.businessManager

import geb.Page

class bmLoginPage extends Page {

	static url = "https://acquitygroup02.alliance-prtnr-na01.dw.demandware.net/on/demandware.store/Sites-Site/default/ViewApplication-DisplayWelcomePage#BM_Callout";

	static at = {
		waitFor {
			title.contains("Demandware Business Manager")
		}
	}

	static content = {
		login {$("input.login_input", name: "LoginForm_Login")}
		password {$("input", name: "LoginForm_Password")}
		logon {$("input.button")}
	}
	
	def loginUser(String user, String pass){
		login << user
		password << pass
		logon.click()
	}

}
