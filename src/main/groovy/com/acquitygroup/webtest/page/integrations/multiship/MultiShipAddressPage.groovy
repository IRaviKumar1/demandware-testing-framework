package com.acquitygroup.webtest.page.integrations.multiship

import com.acquitygroup.webtest.model.Address;
import com.acquitygroup.webtest.module.myaccount.AddressModule;

import geb.Page

class MultiShipAddressPage extends Page {

	static url = "default/Cart-Show"

	static at = {
		title.trim() == "Sites-RefSite-Site"
	}

	static content = {
		addressModule(required: false, wait: true) {module AddressModule}

		continueButton{ $("button", value: startsWith("Continue"), type: "submit") }
		saveButton{ addressModule.addressForm.find("button", name: endsWith("_save"), type: "submit") }
		addNewAddress { addressModule.addressForm.find("") }
		addressSelect(required: false) {addressModule.addressForm.find("select.input-select", name: "dwfrm_multishipping_editAddress_addressList")}
		container {$("div.checkout")}
		//editAddressX{int x -> container.find("tr.cart-row td.shippingaddress div.editaddress a.edit", x)}
		
		editAddressX{String x -> container.find('a[data-lineindex="'+x+'"]')}
		savedAddressSelect (required: false) {String num -> container.find("select.selectbox", name: "dwfrm_multishipping_addressSelection_quantityLineItems_i${num}_addressList")}
		
	}

}
