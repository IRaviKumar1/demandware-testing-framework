package com.acquitygroup.webtest.page.myaccount

import com.acquitygroup.webtest.module.navigation.HeaderModule;

import geb.Page

class MyAccountPage extends Page {

	static url = "default/Account-Show";
	
	static at = {
		waitFor {
			title.equals("My SiteGenesis Account Home")
		}
	}
	static content = {
		primaryHeader { String contents -> $("#primary h1", 0).text().toLowerCase().contains(contents) }
		header { module HeaderModule }
	}
	
}
