package com.acquitygroup.webtest.page.myaccount

import com.acquitygroup.webtest.module.myaccount.LoginModule
import com.acquitygroup.webtest.module.navigation.SearchModule
import com.acquitygroup.webtest.module.navigation.NavigationModule
import geb.Page

class LoginPage extends Page {

    static url = "default/Account-Show"

    static at = {
        mainHeader.text().startsWith("My Account")
    }

    static content = {
        mainHeader { $("div#primary > h1") }

        loginModule { module LoginModule }
		searchModule { module SearchModule }
		navigationModule { module NavigationModule }
    }
}
