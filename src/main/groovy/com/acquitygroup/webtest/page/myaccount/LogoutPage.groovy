package com.acquitygroup.webtest.page.myaccount

import geb.Page

class LogoutPage extends Page {

    static url = "default/Login-Logout"

    static at = {
        title.trim() == "My SiteGenesis Account Login"
    }

}
