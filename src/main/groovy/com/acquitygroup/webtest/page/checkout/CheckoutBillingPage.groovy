package com.acquitygroup.webtest.page.checkout

import com.acquitygroup.webtest.module.myaccount.AddressModule
import com.acquitygroup.webtest.module.myaccount.PaymentsModule
import geb.Page

class CheckoutBillingPage extends Page {

    static url = "default/Cart-Show"

    static at = {
        title.trim() == "SiteGenesis Checkout"
    }

    static content = {
        form { $("form.checkout-billing.address") }
        addressForm { form.find("fieldset", 0) }
        addressModule { module AddressModule }
        saveCard { $("div.form-row input.input-checkbox", name: endsWith("saveCard")) }
        addToAddressBook { addressForm.find("div.form-row input.input-checkbox", name: endsWith("addToAddressBook")) }
        customerEmail { addressForm.find("div.form-row input", name: endsWith("email_emailAddress")) }

        shippingMethodForm { form.find("#shipping-method-list fieldset") }
        shippingMethods { shippingMethodForm.find("input", class: "input-radio") }

        continueButton(to: CheckoutBillingPage) { form.find("button", value: startsWith("Continue"), type: "submit") }

        PaymentMethodID { String selection -> form.find("label", for: "${selection}") }

        creditCardModule { module PaymentsModule }
    }

}
