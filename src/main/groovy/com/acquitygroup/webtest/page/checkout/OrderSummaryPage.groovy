package com.acquitygroup.webtest.page.checkout

import geb.Page

class OrderSummaryPage extends Page {

    static url = "default/COSummary-Submit"

    static at = {
        title.trim().contains("SiteGenesis Checkout Confirmation")
    }

    static content = {
		//TODO: make this not hardcoded!
		//TODO: create order summary module
		//TODO: create verify order details method
        iPhoneDetailsLink(wait: true) { $("a", title: "Apple iPod Touch") }
    }


}
