package com.acquitygroup.webtest.page.checkout

import geb.Page

class CheckoutReviewPage extends Page {

    static url = "default/COBilling-Start"

    static at = {
        placeOrderButton != null
        true
    }

    static content = {
        placeOrderButton(wait: true) { $("button", value: "Submit Order") }
		shippingCostLine {$("tr.order-shipping td", 1)}
		taxesCostLine {$("tr.order-sales-tax td", 1)}
    }

}
