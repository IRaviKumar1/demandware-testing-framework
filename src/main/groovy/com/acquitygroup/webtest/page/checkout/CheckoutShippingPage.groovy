package com.acquitygroup.webtest.page.checkout

import com.acquitygroup.webtest.module.myaccount.AddressModule
import geb.Page

class CheckoutShippingPage extends Page {

    static url = "default/Cart-Show"

    static at = {
        waitFor { title.trim() == "SiteGenesis Checkout" }
    }

    static content = {
        form { $("form.checkout-shipping.address") }

        addressForm { form.find("fieldset", 0) }
        addressModule { module AddressModule }
        useAsBillingCheckbox {
            addressForm.find("div.form-row input.input-checkbox", name: endsWith("useAsBillingAddress"))
        }
        addToAddressBook { addressForm.find("div.form-row input.input-checkbox", name: endsWith("addToAddressBook")) }
        shippingMethodForm { form.find("#shipping-method-list fieldset") }
        shippingMethods { shippingMethodForm.find("input", class: "input-radio") }

        continueButton(to: CheckoutBillingPage) { form.find("button", value: startsWith("Continue"), type: "submit") }

        multiShipButton(required: false) { $("button.shiptomultiplebutton") }
    }

}
