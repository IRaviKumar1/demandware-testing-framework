package com.acquitygroup.webtest.util

import com.acquitygroup.webtest.model.Address
import com.acquitygroup.webtest.model.CreditCard
import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.module.product.ProductDetailsModule
import com.acquitygroup.webtest.page.cart.CartPage
import com.acquitygroup.webtest.page.checkout.CheckoutBillingPage
import com.acquitygroup.webtest.page.checkout.CheckoutReviewPage
import com.acquitygroup.webtest.page.checkout.CheckoutShippingPage
import com.acquitygroup.webtest.page.checkout.OrderSummaryPage
import com.acquitygroup.webtest.page.product.ProductDetailsPage
import geb.Browser

class CheckoutUtil {

    private CheckoutUtil() {
        throw new IllegalAccessException("This is a utility")
    }

    /**
     * Adds product to cart
     * @param browser {@link geb.Browser}
     * @param category {@link Product}
     */
    public static void addProductToCart(Browser browser, Integer addQuantity, Product product) {
        browser.drive {
            to(ProductDetailsPage, product)
            waitFor {
                at(ProductDetailsPage)
                assert title.startsWith(product.getTitle())
                true
            }
            productDetails.setSwatch("Select Color", product.getColor().getDisplay(), ProductDetailsModule.SwatchState.ON)
            productDetails.quantity = ""
            productDetails.quantity << Integer.toString(addQuantity)
            productDetails.addToCart.click()
            waitFor { !productDetails.miniCart.isCartEmpty() }
            waitFor { productDetails.miniCart.isProductInCart(product) }

        }
    }

    /**
     * Brings user through guest checkout process
     * @param browser {@link geb.Browser}
     * @param shipping {@link Address}
     * @param billing {@link Address}
     * @param card {@link CreditCard}
     */
    public static void continueThroughGuestCheckout(Browser browser, Address shipping, Address billing, CreditCard card) {
        browser.drive {
            to(CartPage)
            checkoutAsGuest.click()

            at(CheckoutShippingPage)
            addressModule.fillAddressForm(shipping)
            useAsBillingCheckbox.click()
            continueButton.click()

            at(CheckoutBillingPage)
            waitFor { form.displayed }
            addressModule.fillAddressForm(billing)
            customerEmail = "someone@example.com"
            creditCardModule.setPaymentInfo(card)
            continueButton.click()

            at(CheckoutReviewPage)
            placeOrderButton.click()

            at(OrderSummaryPage)
        }
    }

}