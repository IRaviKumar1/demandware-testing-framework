package com.acquitygroup.webtest.model

/**
 * Model class encapsulating what a Shipment is in a default Demandware site
 */
class Shipment {
	private final String shippingMethod
	private final List<ProductLineItem> productLineItems
	private final Address shippingAddress

	/**
	 * Constructs a full Shipment
	 * @param shippingMethod
	 * @param productLineItems
	 * @param shippingAddress
	 */
	Shipment(String shippingMethod, List<ProductLineItem> productLineItems, Address shippingAddress) {
		this.shippingMethod = shippingMethod
		this.productLineItems = productLineItems
		this.shippingAddress = shippingAddress
	}
	
	String getShippingMethod() {
		return shippingMethod
	}
	
	List<ProductLineItem> getProductLineItems() {
		return productLineItems
	}
	
	Address getShippingAddress() {
		return shippingAddress
	}
	
}
