package com.acquitygroup.webtest.model
/**
 * Model class encapsulating what an Address is in a default Demandware site
 */
class Address {
    private final String addressName
    private final String firstName
    private final String lastName
    private final String address1
    private final String address2
    private final String countryCode
    private final String stateCode
    private final String city
    private final String zipCode
    private final String phone

    /**
     * Constructs a full Address
     * @param addressName
     * @param firstName
     * @param lastName
     * @param address1
     * @param countryCode
     * @param stateCode
     * @param city
     * @param zipCode
     * @param phone
     */
    Address(String addressName, String firstName, String lastName, String address1, String address2,
            String countryCode, String stateCode, String city, String zipCode, String phone) {
        this.addressName = addressName
        this.firstName = firstName
        this.lastName = lastName
        this.address1 = address1
        this.address2 = address2
        this.countryCode = countryCode
        this.stateCode = stateCode
		this.city = city
        this.zipCode = zipCode
        this.phone = phone
    }

    String getAddressName() {
        return addressName
    }

    String getFirstName() {
        return firstName
    }

    String getLastName() {
        return lastName
    }

    String getAddress1() {
        return address1
    }

    String getAddress2() {
        return address2
    }

    String getCountryCode() {
        return countryCode
    }

    String getStateCode() {
        return stateCode
    }

    String getCity() {
        return city
    }

    String getZipCode() {
        return zipCode
    }

    String getPhone() {
        return phone
    }
}
