package com.acquitygroup.webtest.model.category

class URLCategoryLevel extends CategoryLevel {

    URLCategoryLevel(String category, String categoryTitle) {
        super(category, categoryTitle)
    }

    @Override
    String getQueryString() {

        def cgid = "${category}"

        def each = [cgid].findAll() { str ->
            if (str != null) {
                str
            }
        }

        "/" + each.join("/")
    }

}
