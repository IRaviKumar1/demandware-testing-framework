package com.acquitygroup.webtest.model.category

class QueryParamCategoryLevel extends CategoryLevel {

    public QueryParamCategoryLevel(String category, String categoryTitle) {
        super(category, categoryTitle)
    }

    @Override
    String getQueryString() {

        def cgid = "cgid=${category}"

        def each = [cgid].findAll() { str ->
            if (str != null) {
                str
            }
        }

        "?" + each.join("&")
    }

}
