package com.acquitygroup.webtest.model.product

import com.acquitygroup.webtest.model.query.EmptyQueryStrategy
import com.acquitygroup.webtest.model.query.QueryParameterStrategy
import com.acquitygroup.webtest.model.query.Queryable
import com.google.common.collect.Maps

abstract class AbstractProduct implements Queryable {

    protected final ProductColor color
    protected final ProductIdentifier pid
    protected final ProductPrice price
    protected final ProductSize size
    protected final Map<String, ProductAttribute> attributes
    protected final String title
    protected final QueryParameterStrategy queryStrategy

    /**
     * @param title
     * @param color
     * @param pid
     * @param price
     * @param size
     */
    AbstractProduct(String title, ProductColor color, ProductIdentifier pid, ProductPrice price, ProductSize size) {
        this.title = title
        this.color = color
        this.pid = pid
        this.price = price
        this.size = size
        this.queryStrategy = new EmptyQueryStrategy()
        this.attributes = Maps.newHashMap()
        populateInitialAttributes(color, price, size)
    }

    AbstractProduct(String title, ProductColor color, ProductIdentifier pid, ProductPrice price, ProductSize size, QueryParameterStrategy queryStrategy) {
        this.title = title
        this.color = color
        this.pid = pid
        this.price = price
        this.size = size
        this.queryStrategy = queryStrategy
        this.attributes = Maps.newHashMap()
        populateInitialAttributes(color, price, size)
    }

    private void populateInitialAttributes(ProductColor color, ProductPrice price, ProductSize size) {
        if (color != null) {
            attributes.put(color.getKey(), color);
        }
        if (size != null) {
            attributes.put(size.getKey(), size);
        }
        if (price != null) {
            attributes.put(price.getKey(), price);
        }
    }

    boolean isMaster() {
        false
    }

    Map<String, ProductAttribute> getAttributeMap() {
        attributes;
    }

    String getTitle() {
        title
    }

    ProductColor getColor() {
        color
    }

    ProductIdentifier getPid() {
        pid
    }

    ProductPrice getPrice() {
        price
    }

    QueryParameterStrategy getStrategy() {
        queryStrategy
    }

    ProductSize getSize() {
        size
    }

}
