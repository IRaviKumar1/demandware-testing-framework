package com.acquitygroup.webtest.model.account

import com.acquitygroup.webtest.model.Address
import com.acquitygroup.webtest.model.CreditCard
import com.acquitygroup.webtest.model.Order
import com.google.common.collect.Lists
import org.joda.time.DateMidnight
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

/**
 * Model class encapsulating what an Address is in a default Demandware site
 */
class Customer {
    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("mm/dd/yyyy");

    private final String email
    private final String firstName
    private final String lastName
    private final String password
    private final Date birthday
    private final List<CreditCard> paymentMethods
    private final List<Address> addressBook
    private final List<Order> orders

    /**
     * Constructs a full Customer
     * @param addressName
     * @param firstName
     * @param lastName
     * @param address1
     * @param countryCode
     * @param stateCode
     * @param city
     * @param zipCode
     * @param phone
     */
    Customer(String email, String firstName, String lastName, String password, Date birthday, List<CreditCard> paymentMethods, List<Address> addressBook, List<Order> orders) {
        this.email = email
        this.firstName = firstName
        this.lastName = lastName
        this.password = password
        this.birthday = birthday

        // create a new linked list
        this.paymentMethods = (paymentMethods == null) ? Lists.newLinkedList() : Lists.newLinkedList(paymentMethods)
        this.addressBook = (addressBook == null) ? Lists.newLinkedList() : Lists.newLinkedList(addressBook)
        this.orders = (orders == null) ? Lists.newLinkedList() : Lists.newLinkedList(orders)
    }

    String getEmail() {
        return email
    }

    String getFirstName() {
        return firstName
    }

    String getLastName() {
        return lastName
    }

    String getPassword() {
        return password
    }

    /**
     * Returns a pretty print birthday based on mm/dd/yyyy format
     * @return
     */
    String prettyPrintBirthday() {
        fmt.print(new DateMidnight(birthday))
    }

    List<CreditCard> getPaymentMethods() {
        return paymentMethods
    }

    List<Address> getAddressBook() {
        return addressBook
    }

    List<Order> getOrders() {
        return orders
    }
}
