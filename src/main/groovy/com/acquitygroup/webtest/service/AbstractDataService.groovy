package com.acquitygroup.webtest.service

import com.google.common.annotations.VisibleForTesting

class AbstractDataService extends AbstractRemoteService {

    private static final String DATA_RESOURCE = "/data"

    AbstractDataService() {
        super()
    }

    @VisibleForTesting
    AbstractDataService(URI configFilePath) {
        super(configFilePath)
    }

    @Override
    String getResource() {
        DATA_RESOURCE
    }

}
