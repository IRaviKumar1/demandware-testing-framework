package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.model.account.AccountProfile
import com.google.common.annotations.VisibleForTesting
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import org.apache.http.HttpStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class RemoteStoreAccountService extends AbstractStoreService {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteStoreAccountService.class)

    @VisibleForTesting
    RemoteStoreAccountService(URI configFilePath) {
        super(configFilePath)
    }

    public RegisteredAccount registerCustomer(AccountProfile account) {
        def query = getDefaultQueryParameterMap()
        def path = "account/register"

        RegisteredAccount registeredAccount = RegisteredAccount.INVALID

        def registrationObject = [
                "credentials": [
                        "password": account.getPassword(),
                        "username": account.getEmail()
                ],
                "profile": [
                        "email": account.getEmail(),
                        "first_name": account.getFirst(),
                        "last_name": account.getLast(),
                        "birthday": account.getDateOfBirth(),
                        "gender": account.getGender().getMnemonic(),
                        "preferred_locale": account.getPreferredLocale(),
                ]
        ]

        HttpResponseDecorator resp
        try {
            resp = client.post(path: path, query: query, body: registrationObject, requestContentType: ContentType.JSON) as HttpResponseDecorator
            LOG.debug("API::RegisterAccount Response {}", resp.getStatus())
            if (resp.getStatus() == HttpStatus.SC_OK) {
                def data = resp.getData()
                LOG.debug(data.toString())
                if (data != null) {
                    // NOTE: Password is not returned by data API, so re-using password passed in to register
                    AccountProfile profile = new AccountProfile(data.first_name, data.last_name, data.email, account.getPassword(), Gender.getGender(data.gender), data.birthday, data.preferred_locale)
                    registeredAccount = new RegisteredAccount(data.customer_no, data.email, profile)
                }
            }
        } catch (ex) {
            LOG.error("Query for Products in Category failed with [{}] response", ex)
            LOG.debug("Response Data: " + ex.response.getData())
        }

        registeredAccount
    }

}
