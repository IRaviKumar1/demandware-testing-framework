package com.acquitygroup.webtest.module.myaccount

import com.acquitygroup.webtest.model.CreditCard
import geb.Module

class PaymentsModule extends Module {

    static content = {

        paymentForm(required: false) { $("#CreditCardForm") }

        cardName(required: false) { $("#dwfrm_paymentinstruments_creditcards_newcreditcard_owner") }
        cardType(required: false) { $("#dwfrm_paymentinstruments_creditcards_newcreditcard_type") }


        cardNumber(required: false) {
            $("input", name: startsWith("dwfrm_paymentinstruments_creditcards_newcreditcard_number_"))
        }
        cardExpiryMonth(required: false) { $("#dwfrm_paymentinstruments_creditcards_newcreditcard_month") }
        cardExpiryYear(required: false) { $("#dwfrm_paymentinstruments_creditcards_newcreditcard_year") }


        creditCardNumber(required: false) { $("input", name: iEndsWith("creditCard_number")) }
        creditCardName(required: false) { $("input", name: iEndsWith("creditCard_owner")) }
        creditCardType(required: false) { $("select", name: iEndsWith("creditCard_type")) }
        creditCardExpireYear(required: false) { $("select", name: iEndsWith("creditCard_year")) }
        creditCardExpireMonth(required: false) { $("select", name: iEndsWith("creditCard_month")) }
        creditCardCVN(required: false) { $("input", name: iEndsWith("creditCard_cvn")) }



        applyButton(required: false, wait: true) { $(paymentForm).find("#applyBtn") }
        cancelButton(required: false) { $(paymentForm).find(".button-secondary") }
        requiredFieldError(required: false) { $("span.error") }
    }

    /**
     * Fills in an payment on the MyAccount and Registration pages
     * @param payment CreditCard
     */
    def fillPaymentForm(CreditCard payment) {
        cardName << payment.getName()
        cardType << payment.getType()
        cardNumber << payment.getNumber()
        cardExpiryMonth << payment.getExpiryMonth()
        cardExpiryYear << Integer.toString(payment.getExpiryYear())
    }

    def emptyPaymentForm() {
        cardName = ""
        cardNumber = ""
    }

    def setPaymentInfo(CreditCard card) {
        creditCardName << card.getName()
        creditCardNumber << card.getNumber()
        assert creditCardType != null
        creditCardType << card.getType()
        creditCardExpireYear << card.getExpiryYear().toString()
        creditCardExpireMonth << card.getExpiryMonth()
        creditCardCVN << card.getCVV().toString()
    }
}
