package com.acquitygroup.webtest.module.product

import com.acquitygroup.webtest.module.cart.MiniCartModule
import com.acquitygroup.webtest.module.navigation.BreadcrumbModule
import geb.Module
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ProductDetailsModule extends Module {

    private static final Logger LOG = LoggerFactory.getLogger(ProductDetailsModule.class)

//    def color

    static content = {

        productTitle { $("h1", class: "product-name") }
        retailPrice { $("#product-content div.product-price span.price-standard", 0) }
        salePrice { $("#product-content div.product-price span.price-sales", 0) }
        selectedSwatchColorLink { $("ul.swatches.Color li.selected a") }
        swatchColorLink { String color -> $("ul.swatches.Color a", title: startsWith(color)) }
        selectedSwatchColor(cache: false, wait: true, require: false) { String color -> $("ul.swatches.Color li.selected-value", text: contains(color)) }
        ratings { $("div.prSnippetNumberOfRatingsText", 0) }

        primaryImage { $("div.product-primary-image img.primary-image", 0) }
        selectedProductThumbnail { $("#thumbnails li.selected a img") }

        // Swatches
        productSwatch(wait: true) { String label, String swatchValue -> productAttibuteValue(label).find("ul li a.swatchanchor", text: contains(swatchValue)) }
        productSwatchSelected(required: false, wait: false) { String label, String swatchValue -> productAttibuteValue(label).find("ul li.selected a.swatchanchor", text: contains(swatchValue)) }

        productAvailability(required: false, wait: true) { $("div.availability") }
        productInStock(required: false, wait: true) { productAvailability.find("p.in-stock-msg") }

        breadcrumb { module BreadcrumbModule }
        // quantity input
        quantity { $("#Quantity") }
        // clickable things
        addToCart() { $("button", id: "add-to-cart") }
        // mini cart
        miniCart { module MiniCartModule }

        // -=========================================================================================
        // TODO: alyssa This could be a module as well...then use this as BASE so that a module can easily override the base when adding in Page content section
        productDetailsBlock { $("div.product-variations", 0) }

        // get the base, this is ok to wait on
        productAttributes(wait: true, required: true, cache: false) { productDetailsBlock.find("ul") }

        // Considered 'private' for content use
        productAttributeRoot(cache: false) { String attribute -> productAttributes.find("li.attribute span.label", text: iStartsWith(attribute)).parent() }
        // Variation Root[li.attribute] span.label
        productAttibuteLabel(cache: false) { String attribute -> productAttributeRoot(attribute).find("span.label") }
        // Variation Root[li.attribute] ul
        productAttibuteValue(cache: false) { String attribute -> productAttributeRoot(attribute).find("ul") }

        productVariationLabel(wait: true, required: true, cache: false) { String attribute -> productAttributes.find("li.attribute label", text: iStartsWith(attribute)) }
        productVariationValue(wait: true, required: true, cache: false) { String attribute -> productVariationLabel(attribute).next() }

        // TODO: alyssa Not make this hard-coded  ex: iPodMemoryDropdown.value("...")
        iPodMemoryDropdown(cache: false) { $("#va-memorySize") }
        eightGigMemory(cache: false) { iPodMemoryDropdown.find("option", 1) }

    }

    public String getRatingCount() {
        ratings.text().trim().replaceAll("\\D+", "")
    }

    public void setSwatch(String label, String value, SwatchState state) {
        LOG.debug("ProductAttributes present = {}", productAttributes.present);

        boolean isSelected = productSwatchSelected(label, value).present

        LOG.debug("IsSelected [{}={}] = {}", label, value, isSelected)

        // IsSelected && wantOff = click || notSelected && wantOn = click
        if ((isSelected && !state.getState()) || (!isSelected && state.getState())) {
            productSwatch(label, value).click()
        }

        isSelected = productSwatchSelected(label, value).present
        LOG.debug("IsSelected [{}={}] = {}", label, value, isSelected)
    }

    public void setSelectableAttribute(String label, String value) {
        productAttibuteValue(label) << value
    }

    public enum SwatchState {
        ON(true), OFF(false)

        private boolean state

        SwatchState(boolean state) {
            this.state = state
        }

        public boolean getState() {
            state
        }
    }

    /**
     * Updates the quantity then submits the add-to-cart button
     * @param count
     */
    def void addProductToCart(Integer count = 1) {

        quantity = ""
        quantity << Integer.toString(count)

        addToCart.click()

        //waitFor { !miniCart.isCartEmpty() }
    }

}
