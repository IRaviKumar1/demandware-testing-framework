package com.acquitygroup.webtest.module.navigation

import geb.Module
import com.acquitygroup.webtest.page.search.SearchPage

class SearchModule extends Module {

	static content = {
		searchBox  { $("input[name=q]", id:"q") }
		searchButton(to:SearchPage)  { $("input[value='go']") }
	}
		
}
