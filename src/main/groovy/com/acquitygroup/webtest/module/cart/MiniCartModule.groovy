package com.acquitygroup.webtest.module.cart

import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.page.cart.CartPage
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class MiniCartModule extends AbstractCartModule {

    private static final Logger LOG = LoggerFactory.getLogger(MiniCartModule.class)

    static content = {
        //mini-cart-content, only numbers and the decimal
        /**
         * subTotal will return back a String that is ready to be used as a Double
         */
        subTotal(wait: true, required: false, cache: false) { $("div.mini-cart-content div.mini-cart-subtotals span.value").text().replaceAll("[\$ ]+", "") }

        // goto the repeating "mini-cart-name", then find the anchor after we've identified the indexed item
        productTitle(wait: true, required: false, cache: false) { Integer index -> $("div.mini-cart-product div.mini-cart-name", index).find("a").text() }
        productQuantity(wait: true, required: false, cache: false) { Integer index -> $("div.mini-cart-product div.mini-cart-pricing", index).find("span.value").text() }
        productSalePrice(wait: true, required: false, cache: false) { Integer index -> $("div.mini-cart-product div.mini-cart-pricing", index).find("span.mini-cart-price").text().replaceAll("[\$ ]+", "") }

        productLineItemTitle { $("div.mini-cart-name a").text() }
        productList { $("div.mini-cart-product") }

        cartEmptyLabel(wait: "miniCart", required: false, cache: false) { $("div#mini-cart span.mini-cart-empty") }

        miniCartHeader(wait: "miniCart", required: false, cache: false) { $("div.mini-cart-header") }

        viewCartButton(to: CartPage, required: false) { $("a.mini-cart-link-cart.button.button-fancy-medium", title: "View Cart") }
        straightToCheckoutLink(required: false) { $("a.mini-cart-link-checkout") }
    }

    //function to Look for the the given product in the mini-cart
    @Override
    boolean isProductInCart(Product product) {
        boolean found = false

        if (!isCartEmpty()) {
            showMiniCart()

            for (def i = 0; i < productList.size(); i++) {
                if (productTitle(i) == product.getTitle()) {
                    found = true
                    break
                }
            }
        }

        found
    }

    /**
     * TODO: This should take a Product, not an index integer....will fix later
     * @param index
     * @return
     */
    public Integer getCurrentProductCount(Integer index) {
        Integer quantity = 0
        if (!isCartEmpty()) {
            showMiniCart()

            String quantityString = productQuantity(index)
            if (quantityString) {
                quantity = Integer.parseInt(quantityString)
            }
        }
        quantity
    }

    public boolean isCartEmpty() {
        // should timeout when looking for the label if the label is not there (meaning cart is not empty)
        boolean found = ((cartEmptyLabel != null) && (cartEmptyLabel.text() == "0" || cartEmptyLabel.text() == "(0)"))

        found
    }

    public void showMiniCart() {
        $("span.mini-cart-label").jquery.mouseover()
        waitFor { miniCartHeader }
    }

}
