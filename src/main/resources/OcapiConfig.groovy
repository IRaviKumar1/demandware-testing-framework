website {
    //default values
    site = "Sites-RefSite-Site"
    sandbox = "02"
    baseUrl = "https://acquitygroup${sandbox}.alliance-prtnr-na01.dw.demandware.net/s/" + site + "/dw"
    version = "v14_2"
    clientId = System.getenv("DW_CLIENT_PASSWORD") ?: System.getProperty("DW_CLIENT_PASSWORD")
}

environments {
    development {
        website {
            // no overrides
        }
    }
    production {
        website {
            // no overrides
        }
    }
}