package com.acquitygroup.webtest.util;

import java.security.SecureRandom;

/**
 * Provides a simple utility to produce a Secure Random Number
 *
 * REVIEWREQUIRED "Check to see if a seed is needed"
 */
public enum RandomUtil {
    INSTANCE();

    private SecureRandom random;

    RandomUtil() {
        random = new SecureRandom();
    }

    public static int getInt(int max) {
        return INSTANCE.getRandom(max);
    }

    private int getRandom(int max) {
        return random.nextInt(max);
    }
}
