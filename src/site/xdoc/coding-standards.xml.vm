<?xml version="1.0" encoding="UTF-8"?>
<document xmlns="http://maven.apache.org/XDOC/2.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/XDOC/2.0 http://maven.apache.org/xsd/xdoc-2.0.xsd">

    <properties>
        <title>Coding Standards - Demandware Testing Framework</title>
        <author email="mike.ensor@acquitygroup.com">Mike Ensor</author>
    </properties>

    <!-- Optional HEAD element, which is copied as is into the XHTML <head> element -->
    <head>
        <title>${title}</title>
        <meta content="Documentation site for Automated Testing Framework" name="description"/>

        #include("header.vm")
    </head>

    <body>
        <a name="back-to-top" id="back-to-top"></a>
        <h1>Coding Conventions</h1>
        <p>
            Each of the technologies used in this framework come with their own sets of recommended File Conventions and this section is the consolidated recommended
            naming structures while utilizing this framework.
        </p>
        <a name="file-conventions" id="file-conventions"/>
        <section name="File Conventions">
            <subsection name="Testing Files">
                <p>
                    Spock is a
                    <def title="Behavior Driven Development">BDD</def>
                    testing framework that sits on top of JUnit. Each of the Specification files are picked up
                    and run by the Surefire Maven plugin. The Surefire plugin scans the test package looking for files with a specific file contention. If a specification file
                    does not end with the proper suffix, then the file is not picked up and run during the tests.
                </p>
                <dl>
                    <dd>Specification - **/*Specification.groovy</dd>
                    <dt>Single File where the focus of features are on one page or Test Case</dt>
                    <dd>Flow Tests - **/*Flow.groovy</dd>
                    <dt>Flow tests are a Specification but differs by having a larger scope than just one Page.</dt>
                </dl>
            </subsection>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>

        <a name="file-conventions" id="file-conventions"/>
        <section name="Geb Conventions">
            <subsection name="Content Elements">
                <p>
                    Spock is a
                    <def title="Behavior Driven Development">BDD</def>
                    testing framework that sits on top of JUnit. Each of the Specification files are picked up
                    and run by the Surefire Maven plugin. The Surefire plugin scans the test package looking for files with a specific file contention. If a specification file
                    does not end with the proper suffix, then the file is not picked up and run during the tests.
                </p>
                <dl>
                    <dd>Specification - **/*Specification.groovy</dd>
                    <dt>Single File where the focus of features are on one page or Test Case</dt>
                    <dd>Flow Tests - **/*Flow.groovy</dd>
                    <dt>Flow tests are a Specification but differs by having a larger scope than just one Page.</dt>
                </dl>
            </subsection>
            <subsection name="Modules">
                <p>
                    - when to encapsulate, when to extend
                </p>
                <dl>
                    <dd>Specification - **/*Specification.groovy</dd>
                    <dt>Single File where the focus of features are on one page or Test Case</dt>
                    <dd>Flow Tests - **/*Flow.groovy</dd>
                    <dt>Flow tests are a Specification but differs by having a larger scope than just one Page.</dt>
                </dl>
            </subsection>
            <subsection name="Events and Actions">
                <p>
                    -- clicks, mouseover, etc
                </p>
                <dl>
                    <dd>Specification - **/*Specification.groovy</dd>
                    <dt>Single File where the focus of features are on one page or Test Case</dt>
                    <dd>Flow Tests - **/*Flow.groovy</dd>
                    <dt>Flow tests are a Specification but differs by having a larger scope than just one Page.</dt>
                </dl>
            </subsection>
        </section>
        <a href="#back-to-top" class="back-to-top">
            <em>Back To Top</em>
        </a>


        #include("footer.vm")

    </body>
</document>