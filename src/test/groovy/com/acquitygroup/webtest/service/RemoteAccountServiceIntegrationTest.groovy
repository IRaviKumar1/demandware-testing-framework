package com.acquitygroup.webtest.service

import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.model.account.AccountProfile
import com.acquitygroup.webtest.util.RandomUtil
import spock.lang.Specification

class RemoteAccountServiceIntegrationTest extends Specification {

    RemoteAccountService service

    def setup() {
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()

        service = new RemoteAccountService(config)
    }

    def "register customer for a new account"() {
        setup:
        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        AccountProfile customer = new AccountProfile(first, last, email, "p@\$\$w0rd1!", Gender.MALE)

        when:
        RegisteredAccount registeredAccount = service.createAccount(customer)

        then:
        registeredAccount != RegisteredAccount.INVALID

    }

    def "search for a customer"() {
        setup:
        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        when: "create new customer"
        RegisteredAccount registeredAccount = service.createAccount(new AccountProfile(first, last, email, "p@\$\$w0rd1!", Gender.MALE))

        then: "verify that account was created"
        registeredAccount != null
        registeredAccount.getCustomerNumber() != null

        when: "search for the same customer"
        List<RegisteredAccount> accounts = service.search(email)

        then: "verify customer was found"
        accounts.size() > 0
        RegisteredAccount foundAccount = accounts.get(0)
        foundAccount != RegisteredAccount.INVALID
        foundAccount.getCustomerNumber() == registeredAccount.getCustomerNumber()
    }

    def "remove account"() {
        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        AccountProfile customer = new AccountProfile(first, last, email, "p@\$\$w0rd1!", Gender.MALE)
        RegisteredAccount registeredAccount = service.createAccount(customer)

        when:
        service.removeAccount(registeredAccount.getCustomerNumber())

        then:
        !service.isActiveCustomerAccount(registeredAccount.getCustomerNumber())

    }

    def "isActiveCustomerAccount on a customer that is not in the system"() {
        when:
        String customerNumber = "888888888888"

        then:
        !service.isActiveCustomerAccount(customerNumber)
    }

    def "isActiveCustomerAccount where customer is active"() {
        when:
        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        AccountProfile customer = new AccountProfile(first, last, email, "p@\$\$w0rd1!", Gender.MALE)
        RegisteredAccount registeredAccount = service.createAccount(customer)

        then:
        service.isActiveCustomerAccount(registeredAccount.getCustomerNumber())
    }
}
