package com.acquitygroup.webtest.search

import com.acquitygroup.webtest.page.HomePageSearchResults
import com.acquitygroup.webtest.page.search.SearchPage
import com.acquitygroup.webtest.page.search.SearchResultsPage
import geb.spock.GebReportingSpec
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class SearchPageSpecification extends GebReportingSpec {

    private static final Logger LOG = LoggerFactory.getLogger(SearchPageSpecification.class)

    def "when no search phrase is entered the user is taken to the homepage"() {
        given: "go to search results page"
        to SearchResultsPage

        expect: "user is at serach results page"
        at SearchResultsPage

        when: "user clicks search results button"
        searchResultsButton.click()

        then: "user is at home page search results"
        at HomePageSearchResults

        LOG.debug("No Search Phrase test done...")
    }


    def "when an invalid search phrase is entered the results page shows no records found"() {
        given: "go to search results page"
        to SearchResultsPage

        expect: "user is at search results page"
        at SearchResultsPage

        when: "user enters bad query and submits search"
        searchResultsBox << "abcdefNothingShouldShowUpForThisQuery"
        searchResultsButton.click()

        then: "user is at search results page"
        at SearchResultsPage
        assert $("span", class: "no-hits-search-term").text() > ""

        LOG.debug("Invalid Search Phrase test done...")
    }


    def "when a valid search phrase is entered the user is shown the product on the results page"() {
        given: "go to search results page"
        to SearchResultsPage

        expect: "user is search results page"
        at SearchResultsPage

        when: "user enters valid serach query and submits"
        searchResultsBox << "iPod"
        searchResultsButton.click()

        then: "user is at search page"
        at SearchPage
        assert $("span", class: "refinement-header").text() == "REFINE SEARCH"

        LOG.debug("Valid Search Phrase test done...")
    }

}
