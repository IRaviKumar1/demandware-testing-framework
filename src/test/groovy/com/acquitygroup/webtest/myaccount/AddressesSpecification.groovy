package com.acquitygroup.webtest.myaccount

import com.acquitygroup.webtest.model.Address
import com.acquitygroup.webtest.model.account.AccountProfile
import com.acquitygroup.webtest.model.account.Gender
import com.acquitygroup.webtest.model.account.RegisteredAccount
import com.acquitygroup.webtest.page.myaccount.AddressesPage
import com.acquitygroup.webtest.page.myaccount.LoginPage
import com.acquitygroup.webtest.page.myaccount.MyAccountPage
import com.acquitygroup.webtest.service.RemoteAccountService
import com.acquitygroup.webtest.service.RemoteStoreAccountService
import com.acquitygroup.webtest.util.AccountUtil
import com.acquitygroup.webtest.util.RandomUtil
import geb.Browser
import geb.spock.GebReportingSpec
import spock.lang.Shared
import spock.lang.Stepwise

@Stepwise
class AddressesSpecification extends GebReportingSpec {

    @Shared
    protected RemoteStoreAccountService storeAccountService

    @Shared
    protected RemoteAccountService bulkAccountService

    @Shared
    protected RegisteredAccount customer

    @Shared
    protected Address address1

    @Shared
    protected Address address2
    /**
     * Always create one account for the whole specification
     */
    def setupSpec() {
        expect: "create user account"
        URI config = getClass().getResource('/OCapiTestConfig.groovy').toURI()
        storeAccountService = new RemoteStoreAccountService(config)
        bulkAccountService = new RemoteAccountService(config)

        String first = "First"
        String last = "Last"
        String email = "${first}.${last}_" + RandomUtil.getInt(10000) + "@mailinator.com".toLowerCase()

        String password = "p@\$\$w0rd1!"
        AccountProfile account = new AccountProfile(first, last, email, password, Gender.MALE)

        customer = storeAccountService.registerCustomer(account)

        assert customer != RegisteredAccount.INVALID
        assert customer.getProfile().getPassword() == password
        assert customer.getCustomerNumber() != null

        address1 = new Address("address-1", customer.getProfile().getFirst(), customer.getProfile().getLast(), "2503 N Workland Ln", "Apt 201", "US", "ID", "Boise", "83704", "4258675309")
        address2 = new Address("address-1", customer.getProfile().getFirst(), customer.getProfile().getLast(), "203 Mill Creek Ln", "", "US", "ID", "Grangeville", "83530", "4258675309")

        when: "Login to browser with new user"
        to LoginPage
        loginModule.fillAndSubmitLogin(customer.getEmail(), customer.getProfile().getPassword())

        then:
        at MyAccountPage
    }

    /**
     * Logs out a customer
     */
    def cleanupSpec() {
        expect: "log out any user"
        AccountUtil.logout(getBrowser())
        assert bulkAccountService.removeAccount(customer.getCustomerNumber()) == true
    }

    def "user can add a new address"() {
        given: "go to addresses page"
        to AddressesPage

        expect: "user is at addresses page"
        at AddressesPage

        if (!isAddressAvailable(address1))
            addAddress(getBrowser(), address1)
    }

    def "user is presented with error when a required field is not populated"() {
        given: "go to addresses page"
        to AddressesPage

        expect: "user is at addresses page"
        at AddressesPage

        when: "customer clicks to create new address"
        createNewAddress.click()

        and: "customer fills in address form"
        with(addressModule) {
            waitFor { addressForm.present }
        }
        applyButton.click()
        then: "ensure error is displayed"
        waitFor { addressModule.requiredFieldError.present }
    }

    def "user cannot add an address with the same id as existing address"() {
        given: "go to addresses page"
        to AddressesPage

        expect: "user is at addresses page"
        at AddressesPage

        if (!isAddressAvailable(address1))
            addAddress(getBrowser(), address1)

        when: "customer clicks to create new address"
        createNewAddress.click()

        and: "wait for address form to load enter same id as existing address"
        with(addressModule) {
            waitFor { addressForm.present }
        }

        fillAddressForm(address2)
        applyButton.click()
        waitFor { !addressModule.requiredFieldError.displayed }
        applyButton.click()
        then: "ensure error is displayed"
        waitFor { addressModule.formError.displayed }
    }

    def "user can edit an existing address"() {
        given: "go to addresses page"
        to AddressesPage

        expect: "user is at addresses page"
        at AddressesPage

        when: "if address is not available, add one"
        if (!isAddressAvailable(address1))
            addAddress(getBrowser(), address1)

        then: "customer clicks to edit an address"
        editAddress.click()

        and: "wait for address form to load"
        with(addressModule) {
            waitFor { addressForm.present }
        }
        when: "customer edits required fields"
        fillAddressForm(address2)
        addressModule.firstName.click()
        editButton.click()
        then:
        waitFor { isAddressAvailable(address2) }

        when: "verify address information"
        assertAddressInformation(address2)
        then: "user is at addresses page"
        at AddressesPage

    }

    def "user can delete an existing address"() {
        given: "go to addresses page"
        to AddressesPage

        expect: "user is at addresses page"
        at AddressesPage
        if (!isAddressAvailable(address2))
            addAddress(getBrowser(), address2)

        when: "click delete address"
        deleteAddress(address2)

        then: "user goes to addresses page "
        to AddressesPage
        at AddressesPage
        !isAddressAvailable(address2)
    }

    def addAddress(Browser browser, Address localAddress) {
        with(browser) {
            when: "customer clicks to create new address"
            createNewAddress.click()

            and: "when form appears, customer enters required information"
            with(addressModule) {
                waitFor { addressForm.present }
            }
            fillAddressForm(localAddress)
            applyButton.click()
            waitFor { !addressModule.requiredFieldError.displayed }
            applyButton.click()
            then: "new address is added"
            at AddressesPage
        }
        assertAddressInformation(localAddress)
    }

    def assertAddressInformation(Address localAddress) {
        waitFor { isAddressAvailable(localAddress) }
        String addressTitle = miniAddressID.text().toString();
        addressTitle.contains(localAddress.getAddressName())
        String addressName = miniAddressName.text().toString();
        assert addressName.contains(localAddress.getFirstName())
        assert addressName.contains(localAddress.getLastName())
        String addressString = miniAddress.text().toString();
        assert addressString.contains(localAddress.getAddress1())
        assert addressString.contains(localAddress.getAddress2())
        assert addressString.contains(localAddress.getCity())
        assert addressString.contains(localAddress.getStateCode())
        assert addressString.contains(localAddress.getZipCode())
        assert addressString.contains(localAddress.getPhone())
    }

}