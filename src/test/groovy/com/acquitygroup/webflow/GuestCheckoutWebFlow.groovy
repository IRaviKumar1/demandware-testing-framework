package com.acquitygroup.webflow

import com.acquitygroup.webtest.model.Address
import com.acquitygroup.webtest.model.CreditCard
import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.model.product.ProductIdentifier
import com.acquitygroup.webtest.model.product.ProductPrice
import com.acquitygroup.webtest.page.category.CategoryLandingPage
import com.acquitygroup.webtest.page.checkout.OrderSummaryPage
import com.acquitygroup.webtest.page.home.HomePage
import com.acquitygroup.webtest.page.product.ProductDetailsPage
import com.acquitygroup.webtest.util.CheckoutUtil
import geb.spock.GebReportingSpec
import spock.lang.Stepwise

@Stepwise // each Feature will run in succession
class GuestCheckoutWebFlow extends GebReportingSpec {

    protected Address shipping = new Address("", "New", "Belgium", "2503 N Workland Ln", "Apt 201", "US", "ID", "Boise", "83704", "970-867-5309")
    protected Address billing = new Address("", "New", "Belgium", "2503 N Workland Ln", "Apt 201", "US", "ID", "Boise", "83704", "970-867-5309")
    protected CreditCard card = new CreditCard("New Belgium", "Visa", "4111111111111111", "October", 2017, 876)

    def "user can navigate to the home page"() {
        given: "user is going to the home page"
        to HomePage

        expect: "user is at the homepage"
        at HomePage
    }

    def "user can select the electronics category"() {
        given: "user is at the homepage"
        at HomePage

        when: "user clicks on category"
        navigationModule.topLevelNavigation("ELECTRONICS").click()

        then: "user is at category landing page"
        at CategoryLandingPage
    }

    def "user can view more details for an iPod Touch"() {
        given: "user is at category landing page"
        at CategoryLandingPage

        when: "user clicks product tile"
        productDetailsLink("Apple iPod Touch").click()

        then: "user is at product detail page"
        at ProductDetailsPage
    }

    def "user can add an iPodTouch to the mini-cart"() {
        setup: "create product object and head to product detail page"
        Product product = Product.builder(new ProductIdentifier("apple-ipod-touch", "apple-ipod-touch"), "Apple iPod Touch")
                .categoryIdentifier("electronics-digital-media-players")
                .price(new ProductPrice(229.00, 229.00))
                .build()

        to ProductDetailsPage, product

        and: "user is at product detail page"
        at ProductDetailsPage

        when: "user selects product variable"
        productDetails.productVariationValue("Memory Size") << "8 GB"

        then: "ensure product is in stock"
        waitFor {
            productDetails.productInStock.displayed
        }

        then: "add product to cart"
        productDetails.addProductToCart(1)

        and: "ensure product is in cart"
        waitFor { productDetails.miniCart.isProductInCart(product) }

        and: "user is at product details page"
        at ProductDetailsPage
    }

    def "user can checkout"() {
        when: "user continues through checkout process as guest (using checkout util)"
        CheckoutUtil.continueThroughGuestCheckout(getBrowser(), shipping, billing, card)
        then: "user is at checkout summary page"
        at OrderSummaryPage
    }


}
