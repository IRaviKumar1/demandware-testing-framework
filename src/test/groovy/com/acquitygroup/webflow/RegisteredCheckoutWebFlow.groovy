package com.acquitygroup.webflow

import com.acquitygroup.webtest.model.Address
import com.acquitygroup.webtest.model.CreditCard
import com.acquitygroup.webtest.model.account.Customer
import com.acquitygroup.webtest.model.product.Product
import com.acquitygroup.webtest.model.product.ProductIdentifier
import com.acquitygroup.webtest.model.product.ProductPrice
import com.acquitygroup.webtest.page.cart.CartPage
import com.acquitygroup.webtest.page.category.CategoryLandingPage
import com.acquitygroup.webtest.page.checkout.*
import com.acquitygroup.webtest.page.home.HomePage
import com.acquitygroup.webtest.page.product.ProductDetailsPage
import com.acquitygroup.webtest.util.AccountUtil
import com.acquitygroup.webtest.util.RandomUtil
import geb.spock.GebReportingSpec
import spock.lang.Shared
import spock.lang.Stepwise

@Stepwise // each Feature will run in succession
class RegisteredCheckoutWebFlow extends GebReportingSpec {

    @Shared
    protected Customer customer = new Customer("someone_${RandomUtil.getInt(100_000)}_${RandomUtil.getInt(100_000)}@somewhere.com", "Demandware", "Customer", "password12345", null, null, null, null)
    protected Address shipping = new Address("", "Demandware", "Customer", "9713 Shelborne Drive", "", "US", "ID", "Boise", "83709", "970-867-5309")
    protected CreditCard card = new CreditCard("Demandware Customer", "Visa", "4111111111111111", "October", 2017, 876)

    /**
     * Always create one account for the whole specification, log out until at checkout page
     */
    def setupSpec() {
        expect: "create user account and log out"
        AccountUtil.createAccount(getBrowser(), customer)
        AccountUtil.logout(getBrowser())
    }

    /**
     * Logs out a customer after checkout is finished
     * @return
     */
    def cleanupSpec() {
        expect: "log out user"
        AccountUtil.logout(getBrowser())
    }

    def "user can navigate to the home page"() {
        given: "user is going to the home page"
        to HomePage

        expect: "user is at the homepage"
        at HomePage
    }

    def "user can select the electronics category"() {
        given: "go to home page"
        at HomePage

        when: "User clicks top level navigation category"
        navigationModule.topLevelNavigation("ELECTRONICS").click()

        then: "user is at category landing page"
        at CategoryLandingPage
    }

    def "user can view more details for an iPod Touch"() {
        given: "user is at category landing page"
        at CategoryLandingPage

        when: "User clicks a product tile"
        productDetailsLink("Apple iPod Touch").click()

        then: "user is at product detail page"
        at ProductDetailsPage
    }

    def "user can add an iPodTouch to the mini-cart"() {
        setup: "create product object"
        Product product = Product.builder(new ProductIdentifier("apple-ipod-touch", "apple-ipod-touch"), "Apple iPod Touch")
                .categoryIdentifier("electronics-digital-media-players")
                .price(new ProductPrice(229.00, 229.00))
                .build()

        //to ProductDetailsPage, product

        //and:
        at ProductDetailsPage

        when: "User selects a product variant"
        productDetails.productVariationValue("Memory Size") << "8 GB"

        then: "ensure product in in stock"
        waitFor {
            productDetails.productInStock.displayed
        }

        then: "add product to cart"
        productDetails.addProductToCart(1)

        and:
        waitFor { productDetails.miniCart.isProductInCart(product) }

        and: "user is at product details page"
        at ProductDetailsPage
    }

    def "user can view the cart"() {

        given: "User is at product details page"
        at ProductDetailsPage

        when: "user clicks minicart"
        productDetails.miniCart.viewCartButton.click()

        then: "user is tt cart page"
        at CartPage

    }

    def "user can proceed to checkout as a registered user"() {
        given: "user is at cart page"
        at CartPage

        when: "user clicks checkout button"
        checkoutButton.click()

        then: "user is at checkout login page"
        at CheckoutLoginPage
    }

    def "user can login to user account to checkout"() {
        given: "user is at checkout login page"
        at CheckoutLoginPage

        when: "log in user"
        loginUser(customer)

        then: "user is at checkout shipping page"
        at CheckoutShippingPage
    }

    def "user can enter shipping information for their order"() {
        given: "user is at checkout shipping page"
        at CheckoutShippingPage

        when: "user adds address to shipping page"
        withNoAlert { addressModule.fillAddressForm(shipping) } == ("No applicable shipping methods for selection.")
        useAsBillingCheckbox.click()
        addToAddressBook.click()
        continueButton.click()

        then: "user is at checkout billing page"
        waitFor { at CheckoutBillingPage }
    }

    def "user can add billing information"() {
        given: "user is at checkout billing page"
        at CheckoutBillingPage

        when: "user fills out billing information"
        waitFor { form.displayed }
        //addressModule.fillAddressForm(billing)
        customerEmail = customer.getEmail()
        creditCardModule.setPaymentInfo(card)
        //saveCard.click()

        continueButton.click()

        then: "user is at checkout review page"
        at CheckoutReviewPage

    }

    def "user can submit their order"() {
        given: "user is at checkout review page"
        at CheckoutReviewPage

        when: "user clicks place order button"
        placeOrderButton.click()

        then: "user is at order summary page"
        at OrderSummaryPage
    }

}
