import com.acquitygroup.webtest.driver.SauceLabsDriverFactory
import org.openqa.selenium.Capabilities
import org.openqa.selenium.Dimension
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities

import java.util.concurrent.TimeUnit

def siteName = "RefSite"
def sandbox = "02"
baseUrl = "https://acquitygroup${sandbox}.alliance-prtnr-na01.dw.demandware.net/on/demandware.store/Sites-${siteName}-Site/"
reportsDir = "target/test-reports/geb"

def sessionId = null

// Chrome capabilities
def Capabilities capabilities = DesiredCapabilities.chrome()
capabilities.setCapability("chrome.switches", Arrays.asList("--disable-extensions", "--incognito"))

//unexpectedPages = [MissingPage]

waiting {
    timeout = 10
    retryInterval = 0.5

    presets {
        slow {
            timeout = 20
            retryInterval = 1
        }
        quick {
            timeout = 5
        }
        miniCart {
            timeout = 1
        }
    }
}

// Default to ChromeDriver
driver = {

    setChromeEnvironmentVariable()

    new ChromeDriver(capabilities)
}


environments {

    // -Dgeb.env=htmlunit
    htmlunit {
        driver = {
            driver = new HtmlUnitDriver()
            driver.javascriptEnabled = true
            driver
        }
    }

    //-Dgeb.env=firefox
    firefox {
        driver = { new FirefoxDriver() }
    }

    phantom {
        /**  NOT QUITE READY **/
        driver = {
            Capabilities caps = new DesiredCapabilities();
            def buildExecutable = System.getenv("path.to.phantomjs")

            caps.setJavascriptEnabled(true);                //< not really needed: JS enabled by default
            caps.setCapability("takesScreenshot", true);    //< yeah, GhostDriver haz screenshotz!
            caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, buildExecutable);
            caps.setCapability(CapabilityType.SUPPORTS_FINDING_BY_CSS, true);
            caps.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0.2) Gecko/20100101 Firefox/10.0.2")

            ArrayList cliArgsCap = new ArrayList();
            cliArgsCap.add("--web-security=false");
            cliArgsCap.add("--ssl-protocol=any");
            cliArgsCap.add("--ignore-ssl-errors=true");

            caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);

            def driver = new PhantomJSDriver(caps)
            driver.manage().window().setSize(new Dimension(1280, 1024))
            driver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS)
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
            driver
        }
    }

    remote {
        driver = SauceLabsDriverFactory.createSauceDriver("chrome:linux:32")
    }
}

private void setChromeEnvironmentVariable() {
    def chromeDriverPath = System.getenv("webdriver.chrome.driver")

    if (!chromeDriverPath) {
        chromeDriverPath = System.getenv("WEBDRIVER_HOME")
        System.setProperty("webdriver.chrome.driver", chromeDriverPath)
    }
}